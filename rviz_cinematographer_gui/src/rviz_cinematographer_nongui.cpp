
#include <fstream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <signal.h>

#include <ros/ros.h>
#include <ros/package.h>

#include <tf/tf.h>
#include <tf/transform_datatypes.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>

#include <rviz_cinematographer_msgs/CameraMovement.h>
#include <rviz_cinematographer_msgs/CameraTrajectory.h>
#include <rviz_cinematographer_msgs/Record.h>
#include <rviz_cinematographer_msgs/Finished.h>

#include <std_msgs/Empty.h>

#include <nav_msgs/Path.h>

#include <rviz_cinematographer_gui/utils.h>

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/thread.hpp>
#include <yaml-cpp/yaml.h>

#include <spline_library/splines/natural_spline.h>
#include <spline_library/splines/uniform_cr_spline.h>
#include <spline_library/vector.h>

namespace rviz_cinematographer_gui
{

  class RvizCinematographerNonGui
  {
    struct InteractiveMarkerWithDurations
    {
      InteractiveMarkerWithDurations(visualization_msgs::InteractiveMarker&& input_marker,
                                     const double transition_duration,
                                     const double wait_duration = 0.0)
        : marker(input_marker)
        , transition_duration(transition_duration)
        , wait_duration(wait_duration)
      {
      }

      visualization_msgs::InteractiveMarker marker;
      double transition_duration;
      double wait_duration;
    };
    enum
    {
      RISING_INTERPOLATION_SPEED = rviz_cinematographer_msgs::CameraMovement::RISING,
      DECLINING_INTERPOLATION_SPEED = rviz_cinematographer_msgs::CameraMovement::DECLINING,
      FULL_INTERPOLATION_SPEED = rviz_cinematographer_msgs::CameraMovement::FULL,
      WAVE_INTERPOLATION_SPEED = rviz_cinematographer_msgs::CameraMovement::WAVE,
    };


    typedef InteractiveMarkerWithDurations TimedMarker;
    typedef std::list<TimedMarker> MarkerList;
    typedef typename MarkerList::iterator MarkerIterator;

    ros::Publisher camera_trajectory_pub_;
    ros::Publisher view_poses_array_pub_;
    ros::Publisher record_params_pub_;
    geometry_msgs::Pose cam_pose_;
    ros::NodeHandle global_handle_, private_handle_;

    /** @brief Name of currently selected marker. */
    std::string current_marker_name_;

    std::string frame_id_;

    /** @brief Currently maintained list of TimedMarkers. */
    MarkerList markers_;

/////////////////////////////////////////////////////////////////
    public:
    RvizCinematographerNonGui() : global_handle_(), private_handle_("~")
    {
      ROS_INFO_STREAM("RvizNonGui");
    }

    static tf::Vector3 rotateVector(const tf::Vector3& vector,
                             const geometry_msgs::Quaternion& quat)
    {
      tf::Quaternion rotation;
      tf::quaternionMsgToTF(quat, rotation);
      return tf::quatRotate(rotation, vector);
    }

    visualization_msgs::InteractiveMarker makeMarker(double x = 0.0,
                                                     double y = 0.0,
                                                     double z = 0.0)
    {
      visualization_msgs::InteractiveMarker marker;
      marker.header.frame_id = frame_id_;
      marker.name = "marker";
      marker.description = "Marker";
      marker.scale = 2.22f;
      marker.pose.position.x = x;
      marker.pose.position.y = y;
      marker.pose.position.z = z;
      marker.pose.orientation.w = M_SQRT1_2;
      marker.pose.orientation.y = M_SQRT1_2;

      makeBoxControl(marker);

      visualization_msgs::InteractiveMarkerControl pose_control;
      pose_control.orientation.w = M_SQRT1_2;
      pose_control.orientation.x = M_SQRT1_2;
      pose_control.orientation.y = 0;
      pose_control.orientation.z = 0;
      pose_control.orientation_mode = visualization_msgs::InteractiveMarkerControl::FIXED;
      pose_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE;
      marker.controls.push_back(pose_control);

      pose_control.orientation.x = 0;
      pose_control.orientation.y = M_SQRT1_2;
      pose_control.orientation_mode = visualization_msgs::InteractiveMarkerControl::FIXED;
      pose_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE;
      marker.controls.push_back(pose_control);

      pose_control.orientation.y = 0;
      pose_control.orientation.z = M_SQRT1_2;
      pose_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE;
      marker.controls.push_back(pose_control);
      return marker;
    }

    static void prepareSpline(const MarkerList& markers,
                  std::vector<Vector3>& input_eye_positions,
                  std::vector<Vector3>& input_focus_positions,
                  std::vector<Vector3>& input_up_directions)
    {
      Vector3 position;
      for(const auto& marker : markers)
      {
        position[0] = static_cast<float>(marker.marker.pose.position.x);
        position[1] = static_cast<float>(marker.marker.pose.position.y);
        position[2] = static_cast<float>(marker.marker.pose.position.z);
        input_eye_positions.push_back(position);

        tf::Vector3 rotated_vector = rotateVector(tf::Vector3(0, 0, 0.),
                                                  marker.marker.pose.orientation);
        position[0] = position[0] + static_cast<float>(rotated_vector.x());
        position[1] = position[1] + static_cast<float>(rotated_vector.y());
        position[2] = position[2] + static_cast<float>(rotated_vector.z());
        input_focus_positions.push_back(position);
        position[0] = 0;
        position[1] = 0;
        position[2] = 1;

        input_up_directions.push_back(position);
      }
    }

    rviz_cinematographer_msgs::CameraMovement makeCameraMovement()
    {
      rviz_cinematographer_msgs::CameraMovement cm;
      cm.eye.header.stamp = ros::Time::now();
      cm.eye.header.frame_id = frame_id_;
      cm.interpolation_speed = WAVE_INTERPOLATION_SPEED;
      cm.transition_duration = ros::Duration(0);

      cm.up.header = cm.focus.header = cm.eye.header;

      cm.up.vector.x = 0.0;
      cm.up.vector.y = 0.0;
      cm.up.vector.z = 1.0;

      return cm;
    }

    void splineToCamTrajectory(const UniformCRSpline<Vector3>& eye_spline,
                               const UniformCRSpline<Vector3>& focus_spline,
                               const UniformCRSpline<Vector3>& up_spline,
                               const std::vector<double>& transition_durations,
                               const std::vector<double>& wait_durations,
                               const double total_transition_duration,
                               rviz_cinematographer_msgs::CameraTrajectoryPtr trajectory)
    {
      const double frequency = 10;
      const bool smooth_velocity = true;

      // rate to sample from spline and get points
      rviz_cinematographer_msgs::CameraMovement cam_movement = makeCameraMovement();
      double rate = 1.0 / frequency;
      double max_t = eye_spline.getMaxT();
      double total_length = eye_spline.totalLength();
      bool first = true;
      bool last_run = false;
      int current_transition_id = 0;
      int previous_transition_id = 0;
      for(double t = 0.0; t <= max_t;)
      {
        // get position in spline
        auto interpolated_position = eye_spline.getPosition(static_cast<float>(t));
        auto interpolated_focus = focus_spline.getPosition(static_cast<float>(t));
        auto interpolated_up = up_spline.getPosition(static_cast<float>(t));

        cam_movement.eye.point.x = interpolated_position[0];
        cam_movement.eye.point.y = interpolated_position[1];
        cam_movement.eye.point.z = interpolated_position[2];
        cam_movement.focus.point.x = interpolated_focus[0];
        cam_movement.focus.point.y = interpolated_focus[1];
        cam_movement.focus.point.z = interpolated_focus[2];

//        if(!ui_.use_up_of_world_check_box->isChecked())
//        {
//          cam_movement.up.vector.x = interpolated_up[0];
//          cam_movement.up.vector.y = interpolated_up[1];
//          cam_movement.up.vector.z = interpolated_up[2];
//        }
        // else is not necessary - up is already set to default in makeCameraMovement

        bool accelerate = false;
        if(!trajectory->trajectory.empty())
          accelerate = trajectory->trajectory.back().interpolation_speed == DECLINING_INTERPOLATION_SPEED;

        cam_movement.interpolation_speed = (first || accelerate) ? RISING_INTERPOLATION_SPEED
                                                                 : FULL_INTERPOLATION_SPEED;

        // decline at end of trajectory and when reaching next position and velocity is not smoothed
        if((!smooth_velocity && current_transition_id != previous_transition_id) || last_run)
          cam_movement.interpolation_speed = DECLINING_INTERPOLATION_SPEED;

        double transition_duration = 0.0;
        if(smooth_velocity)
        {
          double local_length = eye_spline.arcLength(static_cast<float>(std::max(t - rate, 0.0)), static_cast<float>(t));
          transition_duration = total_transition_duration * local_length / total_length;
        }
        else
          transition_duration = transition_durations[(int)std::floor(std::max(t - rate, 0.0))];

        cam_movement.transition_duration = ros::Duration(transition_duration);


        // recreate movement/marker id to wait after transition if waiting time specified
        current_transition_id = (int)std::floor(t + 0.00001); // magic number needed due to arithmetic imprecision with doubles
        if(!smooth_velocity && current_transition_id != previous_transition_id &&
           wait_durations[previous_transition_id] > 0.01)
        {
          cam_movement.interpolation_speed = DECLINING_INTERPOLATION_SPEED;
          trajectory->trajectory.push_back(cam_movement);

          cam_movement.transition_duration = ros::Duration(wait_durations[previous_transition_id]);
          trajectory->trajectory.push_back(cam_movement);
        }
        else
        {
          trajectory->trajectory.push_back(cam_movement);
        }
        previous_transition_id = current_transition_id;

        ROS_DEBUG_STREAM("t " << t << " max_t " << max_t);

        if(last_run)
          break;

        t += rate;
        if(t > max_t)
        {
          last_run = true;
          t = max_t;
        }

        first = false;
      }
    }

    static void computeDurations(const MarkerList& markers,
                                                  std::vector<double>& transition_durations,
                                                  std::vector<double>& wait_durations,
                                                  double& total_transition_duration)
    {
      const double frequency = 10;
      const bool smooth_velocity = true;

      int counter = 0;
      for(const auto& marker : markers)
      {
        // skip first because this marker is only used for the spline
        // skip second because we don't use the timing of the start marker
        if(counter > 1)
        {
          if(smooth_velocity)
            total_transition_duration += marker.transition_duration;
          else
          {
            transition_durations.push_back(marker.transition_duration / frequency);
            wait_durations.push_back(marker.wait_duration);
          }
        }

        counter++;
      }
    }

    void markersToSplinedCamTrajectory(const MarkerList& markers,
                                                               rviz_cinematographer_msgs::CameraTrajectoryPtr trajectory)
    {
      std::vector<Vector3> input_eye_positions;
      std::vector<Vector3> input_focus_positions;
      std::vector<Vector3> input_up_directions;
      prepareSpline(markers, input_eye_positions, input_focus_positions, input_up_directions);

      // Generate splines
      UniformCRSpline<Vector3> eye_spline(input_eye_positions);
      UniformCRSpline<Vector3> focus_spline(input_focus_positions);
      UniformCRSpline<Vector3> up_spline(input_up_directions);

      std::vector<double> transition_durations;
      std::vector<double> wait_durations;
      double total_transition_duration = 0.0;
      computeDurations(markers, transition_durations, wait_durations, total_transition_duration);

      splineToCamTrajectory(input_eye_positions,
                            input_focus_positions,
                            input_up_directions,
                            transition_durations,
                            wait_durations,
                            total_transition_duration,
                            trajectory);
    }

    void publishRecordParams()
    {
      rviz_cinematographer_msgs::Record record_params;
      record_params.do_record = true;
      record_params.path_to_output = "";
      record_params.frames_per_second = 30;
      record_params.compress = true;
      record_params.add_watermark = true;
      record_params_pub_.publish(record_params);
    }


    void Run()
    {
      ros::NodeHandle ph("/rviz_cinematographer_nongui");
      camera_trajectory_pub_ = ph.advertise<rviz_cinematographer_msgs::CameraTrajectory>("/rviz/camera_trajectory", 1);
      view_poses_array_pub_ = ph.advertise<nav_msgs::Path>("/transformed_path", 1, true);
      record_params_pub_ = ph.advertise<rviz_cinematographer_msgs::Record>("/rviz/record", 1);

      std::string trajectories_file_name;
      private_handle_.param<std::string>("camera_trajectories_file", trajectories_file_name, "");
      private_handle_.param<std::string>("frame_id", frame_id_, "base_link");
      if (trajectories_file_name.empty())
      {
        ROS_ERROR_STREAM("Invalid trajectory file:" << trajectories_file_name);
        ros::shutdown();
      }
      loadTrajectoryFromFile(trajectories_file_name);
      ROS_INFO_STREAM("Loaded " << trajectories_file_name << " with " << markers_.size() << " poses.");
      ROS_INFO_STREAM("Starting Camera Movement.");


      MoveCamera();
    }

    void MoveCamera()
    {
      rviz_cinematographer_msgs::CameraTrajectoryPtr cam_trajectory(new rviz_cinematographer_msgs::CameraTrajectory());
      cam_trajectory->target_frame = frame_id_;
      cam_trajectory->allow_free_yaw_axis = false;
      auto it = std::next(markers_.begin());
      if(0)
      {
        MarkerList markers;
        auto current = it;
        // used spline type uses first an last point but doesn't interpolate between them
        // therefore we add the marker before the current - or itself if there is none before
        if(current == std::prev(markers_.end()))
          markers.push_back(*current);
        else
          markers.push_back(*std::next(current));

        // then we add all other markers
        do
        {
          markers.push_back(*current);
        }
        while(current-- != markers_.begin());
        // and the last one a second time
        markers.push_back(*(markers_.begin()));

        markersToSplinedCamTrajectory(markers, cam_trajectory);
      }
      else
      {
        auto previous = markers_.begin();
        do
        {
          previous--;
          appendMarkerToTrajectory(previous, cam_trajectory, markers_.begin());
        }
        while(previous != markers_.begin());
      }
      ROS_INFO_STREAM("setCurrentFromTo.");
      setCurrentFromTo(*it, *(markers_.begin()));

      // publish cam trajectory
      camera_trajectory_pub_.publish(cam_trajectory);
      ROS_INFO_STREAM("Published " << cam_trajectory->trajectory.size() << " trajectories");
    }

    void setCurrentFromTo(TimedMarker& old_current,
                                                  TimedMarker& new_current)
    {
      // update current marker
      current_marker_name_ = new_current.marker.name;

      // update member list
      new_current.marker.controls[0].markers[0].color.r = 0.f;
      new_current.marker.controls[0].markers[0].color.g = 1.f;
      old_current.marker.controls[0].markers[0].color.r = 1.f;
      old_current.marker.controls[0].markers[0].color.g = 0.f;

    }

    void convertMarkerToCamMovement(const TimedMarker& marker,
                                                            rviz_cinematographer_msgs::CameraMovement& cam_movement)
    {
      cam_movement = makeCameraMovement();
      cam_movement.transition_duration = ros::Duration(marker.transition_duration);

//      if(!ui_.use_up_of_world_check_box->isChecked())
//      {
//        // in the cam frame up is the negative x direction
//        tf::Vector3 rotated_vector = rotateVector(tf::Vector3(-1, 0, 0), marker.marker.pose.orientation);
//        cam_movement.up.vector.x = rotated_vector.x();
//        cam_movement.up.vector.y = rotated_vector.y();
//        cam_movement.up.vector.z = rotated_vector.z();
//      }

      // look from
      cam_movement.eye.point = marker.marker.pose.position;

      // look at
      tf::Vector3 rotated_vector = rotateVector(tf::Vector3(0, 0, -1), marker.marker.pose.orientation);
      cam_movement.focus.point.x = marker.marker.pose.position.x + 20 * rotated_vector.x();
      cam_movement.focus.point.y = marker.marker.pose.position.y + 20 * rotated_vector.y();
      cam_movement.focus.point.z = marker.marker.pose.position.z + 20 * rotated_vector.z();
    }

    void appendMarkerToTrajectory(const MarkerIterator& goal_marker_iter,
                                                          rviz_cinematographer_msgs::CameraTrajectoryPtr& cam_trajectory,
                                                          const MarkerIterator& last_marker_iter)
    {
      rviz_cinematographer_msgs::CameraMovement cam_movement;
      convertMarkerToCamMovement(*goal_marker_iter, cam_movement);

      bool first_marker = cam_trajectory->trajectory.empty();
      bool accelerate = false;
      // accelerate if halted before
      if(!cam_trajectory->trajectory.empty())
        if(cam_trajectory->trajectory.back().interpolation_speed == DECLINING_INTERPOLATION_SPEED ||
           cam_trajectory->trajectory.back().interpolation_speed == WAVE_INTERPOLATION_SPEED)
          accelerate = true;

      cam_movement.interpolation_speed = (first_marker || accelerate) ? RISING_INTERPOLATION_SPEED
                                                                      : FULL_INTERPOLATION_SPEED;

      if(goal_marker_iter == last_marker_iter)
      {
        // if the whole trajectory is between the last two markers, use WAVE, else decline
        if(first_marker || cam_trajectory->trajectory.back().interpolation_speed == DECLINING_INTERPOLATION_SPEED ||
           cam_trajectory->trajectory.back().interpolation_speed == WAVE_INTERPOLATION_SPEED)
          cam_movement.interpolation_speed = WAVE_INTERPOLATION_SPEED;
        else
          cam_movement.interpolation_speed = DECLINING_INTERPOLATION_SPEED;
      }

      // if camera should wait, add another static "movement" to the same pose
      if(goal_marker_iter->wait_duration > 0.01)
      {
        // adapt interpolation speed profile of previous movement to halt at marker we are waiting at
        if(cam_movement.interpolation_speed == RISING_INTERPOLATION_SPEED)
          cam_movement.interpolation_speed = WAVE_INTERPOLATION_SPEED;

        if(cam_movement.interpolation_speed == FULL_INTERPOLATION_SPEED)
          cam_movement.interpolation_speed = DECLINING_INTERPOLATION_SPEED;

        cam_trajectory->trajectory.push_back(cam_movement);

        cam_movement.transition_duration = ros::Duration(goal_marker_iter->wait_duration);
        cam_movement.interpolation_speed = DECLINING_INTERPOLATION_SPEED;
        cam_trajectory->trajectory.push_back(cam_movement);
      }
      else
      {
        cam_trajectory->trajectory.push_back(cam_movement);
      }
    }

    void loadTrajectoryFromFile(const std::string & file_name)
    {
      std::string extension = boost::filesystem::extension(file_name);

      if(extension == ".yaml")
      {
        YAML::Node trajectory = YAML::LoadFile(file_name);
        int count = 0;
        markers_.clear();
        for(const auto& pose : trajectory["rviz_cinematographer_camera_poses"])
        {
          visualization_msgs::InteractiveMarker wp_marker = makeMarker();
          wp_marker.controls[0].markers[0].color.r = 1.f;

          wp_marker.pose.orientation.w = pose["orientation"]["w"].as<double>();
          wp_marker.pose.orientation.x = pose["orientation"]["x"].as<double>();
          wp_marker.pose.orientation.y = pose["orientation"]["y"].as<double>();
          wp_marker.pose.orientation.z = pose["orientation"]["z"].as<double>();

          wp_marker.pose.position.x = pose["position"]["x"].as<double>();
          wp_marker.pose.position.y = pose["position"]["y"].as<double>();
          wp_marker.pose.position.z = pose["position"]["z"].as<double>();

          wp_marker.name = std::to_string(count + 1);
          wp_marker.description = std::to_string(count + 1);

          markers_.emplace_back(TimedMarker(std::move(wp_marker), pose["transition_duration"].as<double>(),
                                            pose["wait_duration"].as<double>()));
          count++;
        }
      }
      else if(extension == ".txt")
      {
        int count = 0;
        markers_.clear();
        std::ifstream infile(file_name);
        std::string line;
        double prev_pose_duration = 0.0;
        while(std::getline(infile, line))
        {
          if(boost::starts_with(line, "#"))
            continue;

          std::vector<std::string> pose_strings;
          boost::split(pose_strings, line, boost::is_any_of(" "), boost::algorithm::token_compress_on);

          if(pose_strings.size() != 8)
          {
            ROS_ERROR_STREAM("Line: " << line
                                      << " contains the wrong number of parameters. Format is: timestamp tx ty tz qx qy qz qw. Number of parameters are "
                                      << (int)pose_strings.size());
            continue;
          }

          visualization_msgs::InteractiveMarker wp_marker = makeMarker();
          wp_marker.controls[0].markers[0].color.r = 1.f;

          double transition_duration = 0.0;
          if(count > 0)
            transition_duration = boost::lexical_cast<double>(pose_strings.at(0)) - prev_pose_duration;

          wp_marker.pose.position.x = boost::lexical_cast<double>(pose_strings.at(1));
          wp_marker.pose.position.y = boost::lexical_cast<double>(pose_strings.at(2));
          wp_marker.pose.position.z = boost::lexical_cast<double>(pose_strings.at(3));

          wp_marker.pose.orientation.x = boost::lexical_cast<double>(pose_strings.at(4));
          wp_marker.pose.orientation.y = boost::lexical_cast<double>(pose_strings.at(5));
          wp_marker.pose.orientation.z = boost::lexical_cast<double>(pose_strings.at(6));
          wp_marker.pose.orientation.w = boost::lexical_cast<double>(pose_strings.at(7));

          wp_marker.name = std::to_string(count + 1);
          wp_marker.description = std::to_string(count + 1);

          markers_.emplace_back(TimedMarker(std::move(wp_marker), transition_duration));

          prev_pose_duration = boost::lexical_cast<double>(pose_strings.at(0));
          count++;
        }
      }
      else
      {
        ROS_ERROR_STREAM("Specified file is neither .yaml nor .txt file.\n File name is: " << file_name);
        return;
      }
    }//loadTrajectoryFromFile

  };

}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "rviz_cine_nongui");

  rviz_cinematographer_gui::RvizCinematographerNonGui app;

  app.Run();

  return 0;
}